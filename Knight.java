package net.codejava;

public class Knight {
	private int baseHP;
	private int wp;

	public Knight(int baseHP, int wp) {
		if (baseHP >= 99 && baseHP <= 999)
			this.baseHP = baseHP;
		if (wp == 0 || wp == 1)
			this.wp = wp;
	}

	public int getBaseHP() {
		return baseHP;
	}

	public int getWp() {
		return wp;
	}

	public double getRealHP() {
		return wp == 1 ? baseHP : baseHP / (double) 10;
	}
}
