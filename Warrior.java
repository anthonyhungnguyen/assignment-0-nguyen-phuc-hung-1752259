package net.codejava;

public class Warrior {
	private int baseHP;
	private int wp;

	public Warrior(int baseHP, int wp) {
		if (baseHP >= 1 && baseHP <= 888)
			this.baseHP = baseHP;
		if (wp == 0 || wp == 1)
			this.wp = wp;
	}

	public int getBaseHP() {
		return baseHP;
	}

	public int getWp() {
		return wp;
	}

	public double getRealHP() {
		return wp == 1 ? baseHP : baseHP / (double) 10;
	}
}
